// Our services tabs

let ul = document.getElementById('tabs');
let nodes = Array.from(ul.children);
let tabContent = document.querySelectorAll('.services-article');

nodes.forEach(function(element) {
    element.addEventListener('click', function(e){
        let index = nodes.indexOf(e.target);

        for (let item = 0; item < nodes.length; item++) {
            nodes[item].className = 'services-tabs-item';
            tabContent[item].className = 'services-article';

            if (index === item) {
                nodes[item].className = 'services-tabs-item active';
                tabContent[item].className = 'services-article active';
            }
        }
    })
});

// Our amazing work filter

let amazingTabs = document.querySelector('.amazing-work-tabs');
let amazingLi = Array.from(amazingTabs.children);
let category = '';

amazingLi.forEach(function(element) {
    element.addEventListener('click', function(e) {
        category = e.target.dataset.category;
        $(".amazing-work-item").removeClass('active');
        e.target.className = 'amazing-work-item active';

        if (category === '') {
            for (let image in images) {
                if (image <= 11) {
                    images[image].className = 'amazing-image-item active';
                } else {
                    images[image].className = 'amazing-image-item';
                }
            }
            loadMoreImages.style.display = 'inline-block';
        } else {
            let i = 0;
            images.forEach(function(element) {

                if(element.dataset.category === category) {
                    element.className = 'amazing-image-item active';
                    i++;
                } else {
                    element.className = 'amazing-image-item';
                }
            });

            if (i <= 12) {
                loadMoreImages.style.display = 'none';
            }
        }
    })
});



// Our amazing work load more button

let images = Array.from(document.querySelectorAll('.amazing-image-item'));

for (let image in images) {
    if (image > 11) {
        break;
    }
    images[image].className = 'amazing-image-item active'
}

let loadMoreImages = document.getElementById('load-more');

loadMoreImages.addEventListener('click', function(e){

    const imagesToShow = images.filter(function(image) {
        if (category === '') {
            return image.className === 'amazing-image-item';
        } else {
            return image.dataset.category === category;
        }
    });

    let count = 12;
    if (imagesToShow.length < 12) {
        count = imagesToShow.length;
    }

    for (let i = 0; i < count; i++) {
        imagesToShow[i].className = 'amazing-image-item active'
    }

    if (imagesToShow[imagesToShow.length-1].className === 'amazing-image-item active') {
        loadMoreImages.style.display = 'none';
    }
});

// Reviews slider

$(document).ready(function(){
    $('.authors-list').slick({
        slidesToShow: 4,
        arrows: true,
        dots: false,
        asNavFor: '.reviews-list',
        focusOnSelect: true,
        adaptiveHeight:true
    });

    $('.reviews-list').slick({
        slidesToShow: 1,
        arrows: false,
        dots: false,
        asNavFor: '.authors-list'
    });

});
